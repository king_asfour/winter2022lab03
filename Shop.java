import java.util.Scanner;

public class Shop{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Scanner readerString = new Scanner(System.in);
		Manga[] userManga = new Manga[4];
		
		for(int i = 0; i < userManga.length;i++){
			Manga myBook = new Manga();
			userManga[i] = myBook;
			
			System.out.println("Who is the author of this manga?");
			userManga[i].author = readerString.nextLine();
			System.out.println(" ");
			
			System.out.println("What is the title of this book?");
			userManga[i].title = readerString.nextLine();
			System.out.println(" ");
			

			System.out.println("How much pages does it contain?");
			userManga[i].pages = reader.nextInt();
			System.out.println(" ");
			if(i < userManga.length - 1){
			System.out.println("-------------------- NEXT BOOK!!! --------------------");
			}
			System.out.println(" ");
		}
		
		System.out.println("The author of the last product is: " + userManga[3].author);
		System.out.println("The title of the last product is: " + userManga[3].title);
		System.out.println("The last product contain " + userManga[3].pages + " pages");
		
		userManga[3].productMethod();
			
		}
}
/*
SOURCE:
Source: GeeksforGeeks
https://www.geeksforgeeks.org/why-is-scanner-skipping-nextline-after-use-of-other-next-functions/

how to use parseInt:
userManga[i].pages = Integer.parseInt(reader.nextInt());
*/